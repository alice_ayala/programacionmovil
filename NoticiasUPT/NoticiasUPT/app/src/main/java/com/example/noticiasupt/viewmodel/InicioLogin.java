package com.example.noticiasupt.viewmodel;

public class InicioLogin {
    public String estado;
    public String usuario;
    public String password;
    public String token;

    public InicioLogin(){

    }
    public InicioLogin(String correo, String password){
        this.usuario=usuario;
        this.password=password;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
