package com.example.noticiasupt;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.example.noticiasupt.Api.api;
import com.example.noticiasupt.servicios.ServicioPeticion;
import com.example.noticiasupt.viewmodel.InicioLogin;
import com.example.noticiasupt.viewmodel.Noticias;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Menu extends AppCompatActivity {
    private TextView Notic;
    private Button NoticiaBoton;

    ServicioPeticion peticion;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);
        Notic=(TextView)findViewById(R.id.txtNoticia);
        NoticiaBoton=(Button)findViewById(R.id.btnNoticias);

        NoticiaBoton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                peticion = api.getApi(Menu.this).create(ServicioPeticion.class);
                NotiNoticias();

            }
        });
    }

    public void NotiNoticias(){
        peticion.NotificacionNoticias().enqueue(new Callback<Noticias>() {
            @Override
            public void onResponse(Call<Noticias> call, Response<Noticias> response) {
                if(response.isSuccessful()){
                    if(response.body().getEstado()){
                        List<Noticias.Datos> detalle = response.body().Detalles;

                        for(Noticias.Datos datos:detalle){

                            Toast.makeText(Menu.this,datos.getTitulo(),Toast.LENGTH_LONG).show();
                        }
                    }
                }
                else{

                    Toast.makeText(Menu.this,"Error en el servidor ",Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<Noticias> call, Throwable t) {

                Toast.makeText(Menu.this,"Error",Toast.LENGTH_LONG).show();

            }
        });
    }
}
