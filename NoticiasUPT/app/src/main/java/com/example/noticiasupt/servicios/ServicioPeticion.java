package com.example.noticiasupt.servicios;

import com.example.noticiasupt.viewmodel.InicioLogin;
import com.example.noticiasupt.viewmodel.Noticias;
import com.example.noticiasupt.viewmodel.Notificacion;
import com.example.noticiasupt.viewmodel.RegistarUsuario;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;

public interface ServicioPeticion {
    @FormUrlEncoded
    @POST("api/crearUsuario")
    Call<RegistarUsuario> Registrar_Usuario(@Field("username")String correo, @Field("password")String password);

    @FormUrlEncoded
    @POST("api/login")
    Call<InicioLogin> login (@Field("username")String usuario, @Field("password")String password);


    @GET("api/todasNot")
    Call<Noticias> NotificacionNoticias();

    @FormUrlEncoded
    @POST("api/crearNotUsuario")
    Call<Notificacion>NotificacionesG(@Field("usuarioId") int usuarioId, @Field("titulo")String titulo,@Field("descripcion")String descripcion);

}
