package com.example.noticiasupt.viewmodel;

public class RegistarUsuario {
    public String estado;
    public String correo;
    public String password;
    public String detalle;

    public RegistarUsuario(){

    }
    public RegistarUsuario(String correo, String password){
        this.correo=correo;
        this.password=password;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getDetalle() {
        return detalle;
    }

    public void setDetalle(String detalle) {
        this.detalle = detalle;
    }
}
