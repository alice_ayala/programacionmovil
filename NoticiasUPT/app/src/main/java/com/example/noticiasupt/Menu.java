package com.example.noticiasupt;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.noticiasupt.Api.api;
import com.example.noticiasupt.servicios.ServicioPeticion;
import com.example.noticiasupt.viewmodel.InicioLogin;
import com.example.noticiasupt.viewmodel.Noticias;
import com.example.noticiasupt.viewmodel.Notificacion;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Menu extends AppCompatActivity {
    private TextView Notic;
    private Button NoticiaBoton;
    private Button Notificaciones;
    private EditText usuarioId;
    private EditText titulo;
    private  EditText descripcion;

    ServicioPeticion peticion;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);
        Notic=(TextView)findViewById(R.id.txtNoticia);
        NoticiaBoton=(Button)findViewById(R.id.btnNoticias);
        Notificaciones=(Button)findViewById(R.id.btnEnviar);
        usuarioId=(EditText)findViewById(R.id.txtId);
        titulo=(EditText)findViewById(R.id.txtTitulo);
        descripcion=(EditText)findViewById(R.id.txtDescripcion);

        NoticiaBoton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                peticion = api.getApi(Menu.this).create(ServicioPeticion.class);
                NotiNoticias();
            }
        });

        Notificaciones.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(usuarioId.getText().toString().isEmpty()|| titulo.getText().toString().isEmpty() || descripcion.getText().toString().isEmpty() ){
                    Toast.makeText(Menu.this,"Campos vacios verifique ",Toast.LENGTH_LONG).show();
                }
                else{
                    ServicioPeticion serviciopeti = api.getApi(Menu.this).create(ServicioPeticion.class);
                    Call<Notificacion> llamada= serviciopeti.NotificacionesG(Integer.parseInt(usuarioId.getText().toString()),titulo.getText().toString(),descripcion.getText().toString());
                    llamada.enqueue(new Callback<Notificacion>() {
                        @Override
                        public void onResponse(Call<Notificacion> call, Response<Notificacion> response) {
                            Notificacion Peticion=response.body();
                            if(response.body()==null){
                                Toast.makeText(Menu.this,"Error intente mas tarde",Toast.LENGTH_LONG).show();
                                return;
                            }
                            if(Peticion.estado =="true"){
                                Toast.makeText(Menu.this,"Envio de notificacion ",Toast.LENGTH_LONG).show();
                            }
                        }

                        @Override
                        public void onFailure(Call<Notificacion> call, Throwable t) {
                            Toast.makeText(Menu.this,"Error  ",Toast.LENGTH_LONG).show();
                        }
                    });
                }
            }
        });
    }

    public void NotiNoticias(){
        peticion.NotificacionNoticias().enqueue(new Callback<Noticias>() {
            @Override
            public void onResponse(Call<Noticias>call,Response<Noticias> response) {
                if(response.isSuccessful())
                {
                    if(response.body().estado)
                    {
                        List<Noticias.Datos> detalle = response.body().detalle;

                        for(Noticias.Datos datos:detalle){
                            Notic.setText(datos.getTitulo());
                        }
                    }
                }
                else{
                    Toast.makeText(Menu.this,"Error en el servidor ",Toast.LENGTH_LONG).show();
                }
            }
            @Override
            public void onFailure(Call<Noticias> call, Throwable t) {
                Toast.makeText(Menu.this,"Error",Toast.LENGTH_LONG).show();

            }
        });
    }
}
