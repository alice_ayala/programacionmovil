package com.example.noticiasupt;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.noticiasupt.Api.api;
import com.example.noticiasupt.servicios.ServicioPeticion;
import com.example.noticiasupt.viewmodel.RegistarUsuario;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Registrar extends AppCompatActivity {
    private Button btnRegistro;
    private EditText Nombre;
    private EditText Con1;
    private EditText Con2;
    private TextView txtRegresar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_crear_cuenta);
        btnRegistro=(Button)findViewById(R.id.btnRegistrar);
        Nombre=(EditText)findViewById(R.id.txtNombre);
        Con1=(EditText)findViewById(R.id.txtCon1);
        Con2=(EditText)findViewById(R.id.txtCon2);
        txtRegresar=(TextView)findViewById(R.id.txtRe);

        btnRegistro.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String cont1,cont2;
                cont1=Con1.getText().toString();
                cont2=Con2.getText().toString();

                if(Nombre.getText().toString().isEmpty() || Con1.getText().toString().isEmpty() || Con2.getText().toString().isEmpty()){
                    Toast.makeText(Registrar.this,"Campos vacios verifique ",Toast.LENGTH_LONG).show();
                }
                else{
                    if(cont1.equals(cont2)){
                        ServicioPeticion servicio = api.getApi(Registrar.this).create(ServicioPeticion.class);
                        Call<RegistarUsuario> RegistrarCall= servicio.Registrar_Usuario(Nombre.getText().toString(),Con1.getText().toString());

                        RegistrarCall.enqueue(new Callback<RegistarUsuario>() {
                            @Override
                            public void onResponse(Call<RegistarUsuario> call, Response<RegistarUsuario> response) {
                                RegistarUsuario peticion=response.body();
                                if (response.body()==null){
                                    Toast.makeText(Registrar.this,"Ocurrio un error con el servidor ",Toast.LENGTH_SHORT).show();
                                    return;
                                }
                                if(peticion.estado=="true"){
                                    startActivity(new Intent(Registrar.this,MainActivity.class));
                                    Toast.makeText(Registrar.this,"Datos enviados correctamente ",Toast.LENGTH_SHORT).show();
                                }
                                else{
                                    Toast.makeText(Registrar.this,peticion.detalle,Toast.LENGTH_SHORT).show();
                                }
                            }
                            @Override
                            public void onFailure(Call<RegistarUsuario> call, Throwable t) {
                                Toast.makeText(Registrar.this,"Error",Toast.LENGTH_LONG).show();
                            }
                        });

                    }
                    else{
                        Toast.makeText(Registrar.this,"Las contraseñas no coinciden ",Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });
        txtRegresar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Registrar.this,MainActivity.class);
                startActivity(intent);
            }
        });
    }
}
