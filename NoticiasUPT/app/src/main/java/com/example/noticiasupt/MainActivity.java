package com.example.noticiasupt;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.noticiasupt.Api.api;
import com.example.noticiasupt.servicios.ServicioPeticion;
import com.example.noticiasupt.viewmodel.InicioLogin;
import com.example.noticiasupt.viewmodel.RegistarUsuario;

import java.util.prefs.Preferences;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {
    private TextView Crear;
    private Button ingresar;
    private EditText usuario;
    private EditText contraseña;
    private String ApiToken;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Crear=(TextView)findViewById(R.id.txtcrear);
        usuario=(EditText)findViewById(R.id.txtNombre);
        contraseña=(EditText)findViewById(R.id.txtPass);
        ingresar=(Button)findViewById(R.id.btnInicio);

        SharedPreferences preferencias=getSharedPreferences("credenciales", Context.MODE_PRIVATE);
        String token=preferencias.getString("TOKEN","");

        if(token!= ""){
            Toast.makeText(MainActivity.this,"Bienvenido nuevamente",Toast.LENGTH_SHORT).show();
            startActivity(new Intent(MainActivity.this,Menu.class));

        }
        ingresar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(usuario.getText().toString().isEmpty()||contraseña.getText().toString().isEmpty())
                {
                    Toast.makeText(MainActivity.this,"Campos vacios verifique ",Toast.LENGTH_LONG).show();
                }
                else{
                    ServicioPeticion servicio=api.getApi(MainActivity.this).create(ServicioPeticion.class);
                    Call<InicioLogin> llamada= servicio.login(usuario.getText().toString(),contraseña.getText().toString());
                    llamada.enqueue(new Callback<InicioLogin>() {
                        @Override
                        public void onResponse(Call<InicioLogin> call, Response<InicioLogin> response) {
                            InicioLogin solicitarPeticion=response.body();
                            if(response.body()==null){
                                Toast.makeText(MainActivity.this,"Ocurrio un error con el servidor, intente mas tarde",Toast.LENGTH_LONG).show();
                            }

                            if(solicitarPeticion.estado=="true"){
                                ApiToken= solicitarPeticion.token;
                                guardarRefrencias();
                                Toast.makeText(MainActivity.this,"Bienvenido",Toast.LENGTH_LONG).show();
                                startActivity(new Intent(MainActivity.this,Menu.class));
                            }
                            else{
                                Toast.makeText(MainActivity.this,"Datos incorrectos",Toast.LENGTH_LONG).show();
                            }
                        }

                        @Override
                        public void onFailure(Call<InicioLogin> call, Throwable t) {

                        }
                    });
                }
            }
        });

        Crear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this,Registrar.class);
                startActivity(intent);
            }
        });

    }
    public void guardarRefrencias () {
        SharedPreferences preferencias = getSharedPreferences("credenciales", Context.MODE_PRIVATE);
        String token = ApiToken;
        SharedPreferences.Editor editor = preferencias.edit();
        editor.putString("TOKEN",token);
        editor.commit();
    }
}
