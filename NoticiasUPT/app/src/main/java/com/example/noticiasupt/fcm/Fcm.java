package com.example.noticiasupt.fcm;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;

import com.example.noticiasupt.MainActivity;
import com.example.noticiasupt.R;
import com.example.noticiasupt.Registrar;
import com.example.noticiasupt.notificacion;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import java.util.Random;

public class Fcm extends FirebaseMessagingService {
    @Override
    public void onNewToken(@NonNull String s) {
        super.onNewToken(s);
        Log.e("token","Mi token es: "+s);
        guardarTokenNuevo(s);
    }

     public  void  guardarTokenNuevo(String s){
        DatabaseReference ref= FirebaseDatabase.getInstance().getReference().child("token");
        ref.child("usuarioId").setValue(s);

     }
    @Override
    public void onMessageReceived(@NonNull RemoteMessage remoteMessage) {
        super.onMessageReceived(remoteMessage);
        String from = remoteMessage.getFrom();
        Log.e("TAG","Mensaje Recibido de: "+from);
        /*

        if(remoteMessage.getNotification() != null){
            Log.e("TAG","Titulo: "+remoteMessage.getNotification().getTitle());
            Log.e("TAG","Body: "+remoteMessage.getNotification().getBody());
        }

         */

        //clave valor
        if(remoteMessage.getData().size() > 0){
            Log.e("TAG","Mi titulo es "+remoteMessage.getData().get("titulo"));
            Log.e("TAG","Mi detalle es "+remoteMessage.getData().get("detalle"));
            Log.e("TAG","Mi color es "+remoteMessage.getData().get("aColor"));

            String titulo = remoteMessage.getData().get("titulo");
            String detalle = remoteMessage.getData().get("titulo");
            mayorQueOreo(titulo,detalle);

        }
    }

    private void mayorQueOreo(String titulo, String detalle) {
        String id="mensaje";
        NotificationManager nm =(NotificationManager)getSystemService(Context.NOTIFICATION_SERVICE);
        NotificationCompat.Builder builder=new NotificationCompat.Builder(this,id);
        if(Build.VERSION.SDK_INT >=Build.VERSION_CODES.O){
            NotificationChannel notificationChannel= new NotificationChannel(id,"nuevo",NotificationManager.IMPORTANCE_HIGH);
            notificationChannel.setShowBadge(true);
            assert nm != null;
            nm.createNotificationChannel(notificationChannel);
        }
        builder.setAutoCancel(true)
                .setWhen(System.currentTimeMillis())
                .setContentTitle(titulo)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setContentText(detalle)
                .setContentIntent(AbrirNotificacion())
                .setContentInfo("nuevo");
        Random random= new Random();
        int idNoti= random.nextInt(8000);

    }
//metodo abrir notificacion
    private PendingIntent AbrirNotificacion() {
        Intent intent = new Intent(getApplicationContext(), notificacion.class);
        intent.putExtra("color","rosa");
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        startActivity(intent);
        return PendingIntent.getActivity(this,0,intent,0);

    }
}


